package org.cplcursos.java.proyectoClase.controladores;

import org.cplcursos.java.proyectoClase.DTO.RespuestaAjax;
import org.cplcursos.java.proyectoClase.entidades.Cliente;
import org.cplcursos.java.proyectoClase.servicios.CliSrvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;

@Controller
public class ClientesCtrl {

    @Autowired
    private CliSrvc clienteSrvc;

    @PostMapping("/flash")
    public String mensajeFlash(RedirectAttributes redirAttr){
        redirAttr.addFlashAttribute("mensaje", "Mensaje 1")
                .addFlashAttribute("clase", "success");
        return "redirect:/clientes/lista";
    }

    @GetMapping("/clientes/lista")
    public String listarClientes(Model model) {
        //List<Cliente> listaCli =new ArrayList<>();
        List<Cliente> listaCli = clienteSrvc.listaCli();
        model.addAttribute("clientes", listaCli);
        return "listaCli";
    }

    @GetMapping("/clientes/lista2")
    public String listarClientes2(Model model) {
        //List<Cliente> listaCli =new ArrayList<>();
        List<Cliente> listaCli = clienteSrvc.listaCli();
        model.addAttribute("clientes", listaCli);
        return "listaCli2";
    }

    @GetMapping("/clientes/datoscli/{id}")
    public String datosCli(Model model, @PathVariable int id) {
        if (clienteSrvc.porId(id).isPresent()){
            model.addAttribute("cliente", clienteSrvc.porId(id).get());
        } else {
            model.addAttribute("mensaje", "No tenemos datos de este cliente");
        }
        return "fichaCli";
    }

    @PostMapping("/clientes/datoscli")
    public String procesaFichaCliente(@ModelAttribute Cliente cliente, Model model) {
        clienteSrvc.actualizar(cliente);
        model.addAttribute("cliente", cliente);
        return "confirmaActualizar";
    }

    /*  Este método devuelve directamente un código de estado HTTP y encapsulado en el cuerpo de la respuesta, un
        objeto RespuestaAjax que puede ampliar la información con su estado y su mensaje

        El borrado puede producir las siguientes respuestas
       	- OK (200)          el registro se ha borrado sin problemas.
		- NO EXISTE (204)   el registro solicitado no existe y no se puede borrar.
		- ERRORBD (502)     se ha producido un error en la BD.
		- ERROR (500)       se ha producido un error inesperado.

		En el cuerpo de la petición se indica el id del cliente a Borrar
     */
    @DeleteMapping("/clientes/borrar")
    @ResponseBody
    public ResponseEntity<RespuestaAjax> borrarCliente(@RequestBody Map<String, String> idCli) {
        try {
            // Si el cliente NO existe
            if (clienteSrvc.porId(Integer.parseInt(idCli.get("id"))).isEmpty()) {
                RespuestaAjax resp = new RespuestaAjax("NO EXISTE", "El cliente " + idCli + " no existe.", "warning");
                //ResponseEntity<RespuestaAjax> rspEnt = new ResponseEntity<>(resp, HttpStatus.NO_CONTENT);
                return ResponseEntity.status(HttpStatus.OK).body(resp);
            }
            // Se borra sin error
            // clienteSrvc.borraCliente(id);
            return ResponseEntity.ok(
                    new RespuestaAjax("BORRADO", "El cliente " + idCli + " se ha eliminado de la bbdd.", "success"));
        }
        catch (Exception e) {
            // Si se produce un error en el servidor.
            return new ResponseEntity<>(
                    new RespuestaAjax("ERROR", "Se ha producido el error: " + e, "danger"),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/clientes/borrar2")
    @ResponseBody
    public ResponseEntity<RespuestaAjax> borrarCli2(@RequestBody Map<String, Integer> idCli) {
        System.out.println(idCli);
        // Se borra sin error
        clienteSrvc.borraCliente(idCli.get("id"));
        return ResponseEntity.ok(new RespuestaAjax("CORRECTO", "El cliente se ha borrado con éxito", "success"));
    }

    @GetMapping("/clientes/datoscli2/")
    public String fichaCli2 (@RequestParam Integer id, Model modelo){
        if(clienteSrvc.porId(id).isPresent()) {
            modelo.addAttribute("cliente", clienteSrvc.porId(id).get());
        } else {
            modelo.addAttribute("error", "No se encuentra el cliente.");
        }
        return "fichaCli";
    }
}
