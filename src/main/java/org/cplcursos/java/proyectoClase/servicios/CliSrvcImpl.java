package org.cplcursos.java.proyectoClase.servicios;

import jakarta.websocket.server.ServerEndpoint;
import org.cplcursos.java.proyectoClase.entidades.Cliente;
import org.cplcursos.java.proyectoClase.repos.RepoCli;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CliSrvcImpl implements CliSrvc{

    @Autowired
    private RepoCli repoCli;

    @Override
    public List<Cliente> listaCli() {
        return repoCli.findAll();
    }

    @Override
    public Optional<Cliente> porId(Integer id) {
        return repoCli.findById(id);
    }

    @Override
    public void actualizar(Cliente cli) {
        repoCli.save(cli);
    }

    @Override
    public void borraCliente(Integer id) {
        repoCli.deleteById(id);
    }
}
