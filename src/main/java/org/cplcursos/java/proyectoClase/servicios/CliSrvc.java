package org.cplcursos.java.proyectoClase.servicios;

import org.cplcursos.java.proyectoClase.entidades.Cliente;

import java.util.List;
import java.util.Optional;

public interface CliSrvc {
    List<Cliente> listaCli();
    Optional<Cliente> porId(Integer id);
    void actualizar(Cliente cli);
    void borraCliente(Integer id);
}
