package org.cplcursos.java.proyectoClase.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RespuestaAjax implements Serializable {
    private String status;
    private String mensaje;
    private String clase;
}
