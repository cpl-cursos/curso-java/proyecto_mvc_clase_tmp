package org.cplcursos.java.proyectoClase.repos;

import org.cplcursos.java.proyectoClase.entidades.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoCli extends JpaRepository<Cliente, Integer> {

}
