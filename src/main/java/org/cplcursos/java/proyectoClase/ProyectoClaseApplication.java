package org.cplcursos.java.proyectoClase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoClaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoClaseApplication.class, args);
	}

}
